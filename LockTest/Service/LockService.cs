using LockTest.Interface;
using Microsoft.Extensions.Caching.Memory;

namespace LockTest.Service;

/// <summary>
///     參考
///     https://raychiutw.github.io/2019/%E9%9A%A8%E6%89%8B-Design-Pattern-5-%E9%9B%99%E9%87%8D%E6%AA%A2%E6%9F%A5%E9%8E%96%E5%AE%9A%E6%A8%A1%E5%BC%8F-Double-Checked-Locking-Pattern/
///     https://www.it145.com/9/80012.html
///     https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/statements/lock
/// </summary>
public class LockService : IServer
{
    private readonly IRepo        _repo;
    private readonly IMemoryCache _cache;
    private readonly object       Lock = new object();

    public LockService(IRepo        repo
                     , IMemoryCache cache)
    {
        _repo  = repo;
        _cache = cache;
    }

    /// <summary>
    ///     第一次呼叫的設定
    /// </summary>
    /// <param name="cycle"></param>
    /// <returns></returns>
    public async Task<string> LocK(int cycle)
    {
        const string cacheKey = "TestLockOne";
        _repo.firstTimeGet(cycle);

        Console.WriteLine($"目前執行次數{cycle}");
        //  取得快取資料
        if (!_cache.TryGetValue(cacheKey, out string result))
        {
            //  會進來代表沒有快取等同於null 
            try
            {
                lock (Lock)
                {
                    //  取得快取資料
                    if (!_cache.TryGetValue(cacheKey, out result))
                    {
                        result = _repo.LockGet(cycle);
                        Console.WriteLine($"鎖定快取取得資料的執行迴圈：{cycle}");

                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            .SetAbsoluteExpiration(TimeSpan.FromMinutes(10));

                        _cache.Set(cacheKey, result, cacheEntryOptions);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }
        }

        return result;
    }

    /// <summary>
    ///     第一次呼叫的設定
    /// </summary>
    /// <returns></returns>
    public string LocKTwo()
    {
        throw new NotImplementedException();
    }
}