using LockTest.Interface;

namespace LockTest.Repo;

public class Repo : IRepo
{
    /// <summary>
    ///     第一次呼叫的設定
    /// </summary>
    /// <returns></returns>
    public string? firstTimeGet(int cycle)
    {
        System.Diagnostics.Debug.WriteLine($"目前執行次數{cycle}");
        return null;
    }

    /// <summary>
    ///     鎖定中的呼叫執行
    /// </summary>
    /// <returns></returns>
    public string LockGet(int cycle)
    {
        
        System.Diagnostics.Debug.WriteLine($"鎖定快取取得資料的執行迴圈：{cycle}");
        return $"鎖定測試{cycle}";
    }
}