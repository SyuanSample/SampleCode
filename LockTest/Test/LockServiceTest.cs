using LockTest.Interface;
using LockTest.Service;
using Microsoft.Extensions.Caching.Memory;
using Moq;

namespace LockTest;

public class Tests
{
    private IMemoryCache _cache;
    private Mock<IRepo>  _repo;
    private LockService  _service;


    [SetUp]
    public void Setup()
    {
        _cache = new MemoryCache(new MemoryCacheOptions());
        _repo  = new Mock<IRepo>();

        _repo
            .Setup(x => x.firstTimeGet(It.IsAny<int>()))
            .Returns((string?) null);

        _repo
            .Setup(x => x.LockGet(It.IsAny<int>()))
            .Returns("鎖定測試");

        _service = new LockService(_repo.Object, _cache);
    }

    [Test]
    public async Task 並行呼叫測試_LockGet應該只呼叫一次()
    {
        var result = string.Empty;


        const int cycle      = 200;
        var       totalCycle = 0;
        var       lsTask     = new List<Task>();

        for (int i = 0
             ; i < cycle
             ; i++)
        {
            //Console.WriteLine($"執行建立第{i+1}條");
            var task = Task.Run
            (
                async () =>
                {
                    await _service.LocK(i);
                }
            );
            lsTask.Add(task);
        }

        Task.Delay(2000);
        await Task.WhenAll(lsTask);
        

        _repo.Verify(x => x.LockGet(It.IsAny<int>()), Times.Once());
        _repo.Verify(x => x.firstTimeGet(It.IsAny<int>()), Times.Exactly(200));
    }
}