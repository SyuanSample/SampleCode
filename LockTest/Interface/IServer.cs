namespace LockTest.Interface;

public interface IServer
{
    /// <summary>
    ///     第一次呼叫的設定
    /// </summary>
    /// <param name="cycle"></param>
    /// <returns></returns>
    public Task<string> LocK(int cycle);
    
    
    /// <summary>
    ///     第一次呼叫的設定
    /// </summary>
    /// <returns></returns>
    public string LocKTwo();
}