namespace LockTest.Interface;

public interface IRepo
{
    /// <summary>
    ///     第一次呼叫的設定
    /// </summary>
    /// <returns></returns>
    public string? firstTimeGet(int cycle);

    /// <summary>
    ///     鎖定中的呼叫執行
    /// </summary>
    /// <returns></returns>
    public string LockGet(int cycle);
}