using Coravel.Invocable;

namespace CoravelQueuePayload.QueueJob;

public class TestQueueWithPayload : IInvocable, IInvocableWithPayload<Guid>
{
    public TestQueueWithPayload()
    {

    }
    /// <summary>Execute the logic/code for this invocable instance.</summary>
    /// <returns></returns>
    public async Task Invoke()
    {
        Console.WriteLine($"Test Queue Job Guid : {this.Payload}  ExecuteTime: {DateTime.Now:u}");
    }

    public Guid Payload { get; set; }
}