namespace EqualityComparer.Model;

/// <summary>
///     遊戲館別
/// </summary>
public class GameClubDto
{
    /// <summary>
    ///     ServerId
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    ///     遊戲桌名
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    ///     排序
    /// </summary>
    public int ServerSort { get; set; }

    /// <summary>
    ///     啟用狀態
    /// </summary>
    public byte Enable { get; set; }

    /// <summary>
    ///     急速/一般
    /// </summary>
    public string ServerProperty { get; set; }

    /// <summary>
    ///     Key
    /// </summary>
    public int KeyId { get; set; }
}