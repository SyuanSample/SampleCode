namespace EqualityComparer.Model;

/// <summary>
///     遊戲館別
/// </summary>
public class FrontendGameClubDto
{
    /// <summary>
    ///     Key
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    ///     排序設定
    /// </summary>
    public int? Sort { get; set; }

    /// <summary>
    ///     館名
    /// </summary>
    public string ClubName { get; set; }

    /// <summary>
    ///     館別Code
    /// </summary>
    public string ClubCode { get; set; }

    /// <summary>
    ///     遊戲桌清單
    /// </summary>
    public List<GameClubDto> Server { get; set; }
}