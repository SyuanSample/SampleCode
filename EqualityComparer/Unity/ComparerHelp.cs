using EqualityComparer.Model;

namespace EqualityComparer.Unity;

public static class ComparerHelp
{
    public class ClubComparer : IEqualityComparer<FrontendGameClubDto>
    {
        public bool Equals(FrontendGameClubDto x
                         , FrontendGameClubDto y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null))
            {
                return false;
            }

            if (ReferenceEquals(y, null))
            {
                return false;
            }

            if (x.GetType() != y.GetType())
            {
                return false;
            }

            var property = x.Sort == y.Sort && x.ClubName == y.ClubName;

            var server = x.Server.Except(y.Server, new ServerComparer());

            var ser = !server.Any();

            return property
                && ser;
        }

        public int GetHashCode(FrontendGameClubDto obj)
        {
            var result = HashCode.Combine(obj.Sort, obj.ClubName, obj.Server.Count);
            Console.WriteLine(result);

            return HashCode.Combine(obj.Sort, obj.ClubName, obj.Server.Count);
        }
    }

    public class ServerComparer : IEqualityComparer<GameClubDto>
    {
        public bool Equals(GameClubDto x
                         , GameClubDto y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null))
            {
                return false;
            }

            if (ReferenceEquals(y, null))
            {
                return false;
            }

            if (x.GetType() != y.GetType())
            {
                return false;
            }

            return x.Id == y.Id
                   && x.ServerSort == y.ServerSort
                   && x.KeyId == y.KeyId;
        }

        public int GetHashCode(GameClubDto obj)
        {
            return HashCode.Combine(obj.Id, obj.ServerSort, obj.KeyId);
        }
    }
}