﻿// See https://aka.ms/new-console-template for more information

using EqualityComparer.Model;
using EqualityComparer.Unity;

Console.WriteLine("Hello, World!");

var source  = BaseDataInit();
var request = RequestDataInit();

Console.WriteLine("比對開始");
var diff = request.Except(source, new ComparerHelp.ClubComparer());

Console.WriteLine("比對結束");
var result = diff.ToList();
Console.ReadKey();

/// <summary>
/// 
/// </summary>
/// <returns></returns>
static List<FrontendGameClubDto> RequestDataInit()
{
    var data = new List<FrontendGameClubDto>
    {
        new FrontendGameClubDto()
        {
            Id = 1, Sort = 1, ClubName = "TestClubA", ClubCode = "A", Server = new List<GameClubDto>
            {
                new GameClubDto() {ServerSort = 1, KeyId = 1}
              , new GameClubDto() {ServerSort = 2, KeyId = 2} 
                      , new GameClubDto() {ServerSort = 3, KeyId = 5}/*
                      , new GameClubDto() {ServerSort = 4, KeyId = 3}
                      , new GameClubDto() {ServerSort = 5, KeyId = 4},*/
            }
        }
        /* , new FrontendGameClubDto()
           {
               Id = 2, Sort = 2, ClubName = "TestClubB", ClubCode = "B", Server = new List<GameClubDto>
               {
                   new GameClubDto() {ServerSort = 1, KeyId = 1}
                 , new GameClubDto() {ServerSort = 2, KeyId = 4}
                 , new GameClubDto() {ServerSort = 3, KeyId = 3}
                 , new GameClubDto() {ServerSort = 4, KeyId = 2}
                 , new GameClubDto() {ServerSort = 5, KeyId = 5},
               }
           }
         , new FrontendGameClubDto()
           {
               Id = 3, Sort = 3, ClubName = "TestClubC", ClubCode = "C", Server = new List<GameClubDto>
               {
                  new GameClubDto() {ServerSort = 4, KeyId = 2}
                 , new GameClubDto() {ServerSort = 5, KeyId = 1},
               }
           }*/
    };

    return data;
}

static List<FrontendGameClubDto> BaseDataInit()
{
    var data = new List<FrontendGameClubDto>
    {
        new FrontendGameClubDto()
        {
            Id = 1, Sort = 1, ClubName = "TestClubA", ClubCode = "A", Server = new List<GameClubDto>
            {
                new GameClubDto() {ServerSort = 1, KeyId = 1}
              , new GameClubDto() {ServerSort = 2, KeyId = 2} /*
                      , new GameClubDto() {ServerSort = 3, KeyId = 3}
                      , new GameClubDto() {ServerSort = 4, KeyId = 4}
                      , new GameClubDto() {ServerSort = 5, KeyId = 5},*/
            }
        }
        /* , new FrontendGameClubDto()
           {
               Id = 2, Sort = 2, ClubName = "TestClubB", ClubCode = "B", Server = new List<GameClubDto>
               {
                   new GameClubDto() {ServerSort = 1, KeyId = 5}
                 , new GameClubDto() {ServerSort = 2, KeyId = 4}
                 , new GameClubDto() {ServerSort = 3, KeyId = 3}
                 , new GameClubDto() {ServerSort = 4, KeyId = 2}
                 , new GameClubDto() {ServerSort = 5, KeyId = 1},
               }
           }
         , new FrontendGameClubDto()
           {
               Id = 3, Sort = 3, ClubName = "TestClubC", ClubCode = "C", Server = new List<GameClubDto>
               {
                  new GameClubDto() {ServerSort = 4, KeyId = 2}
                 , new GameClubDto() {ServerSort = 5, KeyId = 1},
               }
           }*/
    };

    return data;
}