namespace ExcuteConsole.Model;

public class TransactionDto
{
    /// <summary>
    ///     Table Key
    /// </summary>
    public long LogId { get; set; }

    /// <summary>
    ///     交易ID
    /// </summary>
    public string TransactionId { get; set; } = null!;

    /// <summary>
    ///     注單編號
    /// </summary>
    public string? Txno { get; set; }

    /// <summary>
    ///     公司Code
    /// </summary>
    public string Company { get; set; } = null!;

    /// <summary>
    ///     系統Code
    /// </summary>
    public string SystemCode { get; set; } = null!;

    /// <summary>
    ///     站台Code
    /// </summary>
    public string WebId { get; set; } = null!;

    /// <summary>
    ///  會員Account
    /// </summary>
    public string MemberAccount { get; set; } = null!;

    /// <summary>
    /// 4XX: 失敗 200 :成功
    /// </summary>
    public short Status { get; set; }

    /// <summary>
    ///     狀態
    ///     1 = 下注
    ///     2 = 取消
    ///     3 = 還錢
    /// </summary>
    public short Type { get; set; }

    /// <summary>
    ///     請求的Guid
    /// </summary>
    public string RequestId { get; set; } = null!;

    /// <summary>
    ///     會員名稱
    /// </summary>
    public string Account { get; set; } = null!;

    /// <summary>
    ///     可用餘額
    /// </summary>
    public decimal Balance { get; set; }

    /// <summary>
    ///     資料建立時間
    /// </summary>
    public DateTime CreateDatetime { get; set; }

    /// <summary>
    ///     Game Server Id
    /// </summary>
    public string ServerId { get; set; } = null!;

    /// <summary>
    ///     遊戲類型
    /// </summary>
    public string GameName { get; set; } = null!;

    /// <summary>
    ///     輪號
    /// </summary>
    public string NoRun { get; set; } = null!;

    /// <summary>
    ///     局號
    /// </summary>
    public string NoActive { get; set; } = null!;

    /// <summary>
    ///     錯誤Id
    /// </summary>
    public int ErrorMsgId { get; set; }

    /// <summary>
    ///     錯誤訊息
    /// </summary>
    public string ErrorMessage { get; set; } = null!;

    /// <summary>
    ///     下注點數
    /// </summary>
    public decimal Point { get; set; }

    /// <summary>
    ///     對應的下注ID
    /// </summary>
    public string ReferenceId { get; set; } = null!;


    /// <summary>
    ///     Company + System + Web + Account + ReferenceId
    /// </summary>
    public int HashCode { get; set; }
}