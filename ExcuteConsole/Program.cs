﻿// See https://aka.ms/new-console-template for more information


using ExcuteConsole.Model;
using MessageService.Dependencyinjections;
using MessageService.Enums;
using MessageService.Factories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var builder = Host.CreateDefaultBuilder(args)
                  .ConfigureServices
                  (
                      service => { service.AddMessageService(); }
                  );

var app = builder.Build();

var _factory = app.Services.GetService<MsgFactory>();

var service = _factory.GetService(MsgServicesEnum.Slack);

var data = new TransactionDto()
{
  LogId   = 1, Txno             = "BT22060700000001", MemberAccount = "TestCase02", Status = -1, Type = 1
, Account = "Case002", HashCode = -2113420541
};

var temp = $@"
        {{
          ""type"": ""home"",
          ""blocks"": [
            {{
              ""type"": ""section"",
              ""text"": {{
                ""type"": ""mrkdwn"",
                ""text"": ""錯誤資料""
              }}
            }}
          ],
          ""attachments"": [
            {{
              ""color"": ""#36a64f"",
              ""blocks"": [
                {{
                  ""type"": ""section"",
                  ""fields"": [
                    {{
                      ""type"": ""mrkdwn"",
                      ""text"": ""*公司資訊*\n Company: {data.Account} \n SystemCode: {data.SystemCode} \n WebId: {data.WebId}""
                    }},
                    {{
                      ""type"": ""mrkdwn"",
                      ""text"": ""*會員資訊*\n MemberAccount: {data.MemberAccount} \n Account: {data.Account}""
                    }}
                  ]
                }},
                {{
                  ""type"": ""divider""
                }},
                {{
                  ""type"": ""section"",
                  ""fields"": [
                    {{
                      ""type"": ""mrkdwn"",
                      ""text"": ""*遊戲資訊*\n ServerId: {data.ServerId} \n GameName: {data.GameName} \n NoRun: {data.NoRun} \n NoActive: {data.NoActive}""
                    }},
                    {{
                      ""type"": ""mrkdwn"",
                      ""text"": ""*下注資訊*\n TxNo: {data.Txno} \n Point: {data.Point} \n balance:{data.Balance}""
                    }}
                  ]
                }},
                {{
                  ""type"": ""divider""
                }},
                {{
                  ""type"": ""section"",
                  ""text"": {{
                    ""type"": ""mrkdwn"",
                    ""text"": ""*其他資訊*\n requestId: {data.RequestId} \n createDatetime: {data.CreateDatetime} \n referenceId: {data.ReferenceId}""
                  }}
                }}
              ]
            }}
          ]
        }}
    ";

service.SendMessageAsync(temp);

Console.WriteLine("Finish");

Console.ReadKey();