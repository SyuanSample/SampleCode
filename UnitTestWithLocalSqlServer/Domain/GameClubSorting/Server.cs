﻿using System;
using System.Collections.Generic;

namespace UnitTestWithSqlite.Domain.GameClubSorting
{
    /// <summary>
    /// 伺服器(機台)
    /// </summary>
    public partial class Server
    {
        public Server()
        {
            FrontendGameClubMappingServers = new HashSet<FrontendGameClubMappingServer>();
        }

        /// <summary>
        /// 伺服器(機台)Id
        /// </summary>
        public string Id { get; set; } = null!;
        public int? ServerNo { get; set; }
        /// <summary>
        /// 遊戲Id
        /// </summary>
        public int GameNo { get; set; }
        /// <summary>
        /// 館別Id
        /// </summary>
        public int LobbyNo { get; set; }
        /// <summary>
        /// 桌別
        /// </summary>
        public string? Tag { get; set; }
        public string? DeskNo { get; set; }
        /// <summary>
        /// 伺服器(機台)名稱
        /// </summary>
        public string Name { get; set; } = null!;
        /// <summary>
        /// 是否啟用
        /// </summary>
        public byte Enable { get; set; }
        public string? ServerUrl1 { get; set; }
        public string? ServerUrl2 { get; set; }
        public string? ServerPort { get; set; }
        public string? HistoryVideoUrl1 { get; set; }
        public string? HistoryVideoUrl2 { get; set; }
        public string? HistoryVideoUrl3 { get; set; }
        public string? ChangeCardUrl { get; set; }
        public string? KeyServerId { get; set; }
        public string? ServerProperty { get; set; }
        public string? StevenCode { get; set; }
        public string? GsurlPath { get; set; }
        public byte KeyId { get; set; }

        public virtual ICollection<FrontendGameClubMappingServer> FrontendGameClubMappingServers { get; set; }
    }
}
