﻿using System;
using System.Collections.Generic;

namespace UnitTestWithSqlite.Domain.GameClubSorting
{
    public partial class FrontendGameClub
    {
        public FrontendGameClub()
        {
            FrontendGameClubMappingServers = new HashSet<FrontendGameClubMappingServer>();
        }

        /// <summary>
        /// 資料表Key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 館別的排序依據
        /// </summary>
        public int? Sort { get; set; }
        /// <summary>
        /// 館別的名稱
        /// </summary>
        public string ClubName { get; set; } = null!;
        /// <summary>
        /// 館別的Code
        /// </summary>
        public string ClubCode { get; set; } = null!;

        public virtual ICollection<FrontendGameClubMappingServer> FrontendGameClubMappingServers { get; set; }
    }
}
