﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace UnitTestWithSqlite.Domain.GameClubSorting
{
    public partial class GameClubSortingContext : DbContext
    {
        public GameClubSortingContext()
        {
        }

        public GameClubSortingContext(DbContextOptions<GameClubSortingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FrontendGameClub> FrontendGameClubs { get; set; } = null!;
        public virtual DbSet<FrontendGameClubMappingServer> FrontendGameClubMappingServers { get; set; } = null!;
        public virtual DbSet<Server> Servers { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FrontendGameClub>(entity =>
            {
                entity.ToTable("FrontendGameClub");

                entity.Property(e => e.Id).HasComment("資料表Key");

                entity.Property(e => e.ClubCode)
                    .HasMaxLength(20)
                    .HasComment("館別的Code");

                entity.Property(e => e.ClubName)
                    .HasMaxLength(20)
                    .HasComment("館別的名稱");

                entity.Property(e => e.Sort)
                    .HasDefaultValueSql("((0))")
                    .HasComment("館別的排序依據");
            });

            modelBuilder.Entity<FrontendGameClubMappingServer>(entity =>
            {
                entity.ToTable("FrontendGameClubMappingServer");

                entity.Property(e => e.Id).HasComment("Key");

                entity.Property(e => e.ClubId).HasComment("館ID");

                entity.Property(e => e.ServerKeyId).HasComment("ServerID");

                entity.Property(e => e.ServerSort).HasComment("Server排序");

                entity.HasOne(d => d.Club)
                    .WithMany(p => p.FrontendGameClubMappingServers)
                    .HasForeignKey(d => d.ClubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FrontendGameClubMpaingServer_FrontendGameClub");

                entity.HasOne(d => d.ServerKey)
                    .WithMany(p => p.FrontendGameClubMappingServers)
                    .HasPrincipalKey(p => p.KeyId)
                    .HasForeignKey(d => d.ServerKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FrontendGameClubMpaingServer_Server");
            });

            modelBuilder.Entity<Server>(entity =>
            {
                entity.ToTable("Server");

                entity.HasComment("伺服器(機台)");

                entity.HasIndex(e => e.KeyId, "UQ__Server__21F5BE46699629C8")
                    .IsUnique();

                entity.HasIndex(e => e.ServerNo, "UQ__Server__C56B13542BC0C14E")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("伺服器(機台)Id");

                entity.Property(e => e.ChangeCardUrl)
                    .HasMaxLength(256)
                    .HasColumnName("ChangeCardURL");

                entity.Property(e => e.DeskNo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Enable)
                    .HasDefaultValueSql("((1))")
                    .HasComment("是否啟用");

                entity.Property(e => e.GameNo).HasComment("遊戲Id");

                entity.Property(e => e.GsurlPath)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("GSUrlPath");

                entity.Property(e => e.HistoryVideoUrl1)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("HistoryVideoURL1");

                entity.Property(e => e.HistoryVideoUrl2)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("HistoryVideoURL2");

                entity.Property(e => e.HistoryVideoUrl3)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("HistoryVideoURL3");

                entity.Property(e => e.KeyId).ValueGeneratedOnAdd();

                entity.Property(e => e.KeyServerId)
                    .HasMaxLength(50)
                    .HasColumnName("KeyServerID");

                entity.Property(e => e.LobbyNo).HasComment("館別Id");

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .HasComment("伺服器(機台)名稱");

                entity.Property(e => e.ServerPort)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ServerProperty)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ServerUrl1)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ServerURL1")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ServerUrl2)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ServerURL2")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.StevenCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Tag)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')")
                    .HasComment("桌別");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
