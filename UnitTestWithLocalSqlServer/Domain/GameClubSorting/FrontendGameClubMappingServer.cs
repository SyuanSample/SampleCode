﻿using System;
using System.Collections.Generic;

namespace UnitTestWithSqlite.Domain.GameClubSorting
{
    public partial class FrontendGameClubMappingServer
    {
        /// <summary>
        /// Key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 館ID
        /// </summary>
        public int ClubId { get; set; }
        /// <summary>
        /// ServerID
        /// </summary>
        public byte ServerKeyId { get; set; }
        /// <summary>
        /// Server排序
        /// </summary>
        public byte ServerSort { get; set; }

        public virtual FrontendGameClub Club { get; set; } = null!;
        public virtual Server ServerKey { get; set; } = null!;
    }
}
