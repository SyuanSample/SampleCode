﻿using System;
using System.Collections.Generic;

namespace UnitTestWithSqlite.Domain.Wallets
{
    /// <summary>
    /// 錢包
    /// </summary>
    public partial class Wallet
    {
        /// <summary>
        /// 會員Id
        /// </summary>
        public long MemberId { get; set; }
        /// <summary>
        /// 目前的點數
        /// </summary>
        public decimal Balance { get; set; }
        public string? GameToken { get; set; }
        public short KickType { get; set; }
        public bool? IsTest { get; set; }
        public string? SessionNo { get; set; }
        public bool? IsOpen { get; set; }
        public short? LobbyKickType { get; set; }
        public bool? IsSingleWalletLock { get; set; }
    }
}
