﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace UnitTestWithSqlite.Domain.Wallets
{
    public partial class WalletContext : DbContext
    {
        public WalletContext()
        {
        }

        public WalletContext(DbContextOptions<WalletContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Wallet> Wallets { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Wallet>(entity =>
            {
                entity.HasKey(e => e.MemberId);

                entity.ToTable("Wallet");

                entity.HasComment("錢包");

                entity.HasIndex(e => e.LobbyKickType, "IX_Wallet");

                entity.Property(e => e.MemberId)
                    .ValueGeneratedNever()
                    .HasComment("會員Id");

                entity.Property(e => e.Balance)
                    .HasColumnType("money")
                    .HasComment("目前的點數");

                entity.Property(e => e.GameToken)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsTest).HasDefaultValueSql("((0))");

                entity.Property(e => e.LobbyKickType).HasDefaultValueSql("((0))");

                entity.Property(e => e.SessionNo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.HasSequence("RXNumber")
                .HasMin(1)
                .HasMax(99999999)
                .IsCyclic();

            modelBuilder.HasSequence("TXNumber")
                .HasMin(1)
                .HasMax(99999999)
                .IsCyclic();

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
