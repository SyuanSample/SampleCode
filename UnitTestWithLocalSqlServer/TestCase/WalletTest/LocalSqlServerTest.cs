using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using UnitTestWithSqlite.Domain.Wallets;

namespace UnitTestWithSqlite.TestCase.Wallet;

public partial class SqliteTest
{
    private WalletContext    _wallet;
    private SqliteConnection _coon;

    [SetUp]
    public async Task Setup()
    {
        const string coon
            = "Server=localhost,1433;Database=Wallet;User Id=sa;Password=1qaz@WSX;TrustServerCertificate=true;Encrypt=true";

        var walletOptions = new DbContextOptionsBuilder<WalletContext>().UseSqlServer(coon);
        this._wallet = new WalletContext(walletOptions.Options);
        await _wallet.Database.EnsureDeletedAsync();
        await _wallet.Database.EnsureCreatedAsync();
    }

    [TearDown]
    public async Task TearDown()
    {
        await _wallet.Database.EnsureDeletedAsync();
    }

    [Test]
    public async Task BulkUpdate功能測試()
    {
        #region 基本資料建立與驗證

        var baseData = new List<Domain.Wallets.Wallet>();

        for (var i = 1
             ; i <= 5000
             ; i++)
        {
            var data = new Domain.Wallets.Wallet()
            {
                MemberId = i, Balance = 1000, KickType = 0, IsSingleWalletLock = true
            };

            baseData.Add(data);
        }

        await _wallet.AddRangeAsync(baseData);
        await _wallet.SaveChangesAsync();

        var result = _wallet.Wallets.Any(w => w.IsSingleWalletLock == true);

        //  驗證資料建立OK
        result.Should()
              .Be(true);

        #endregion

        var id = baseData.Select(b => b.MemberId)
                         .ToList();

        _wallet.Wallets.Where
               (
                   w =>
                       id.Contains(w.MemberId)
                       & w.IsSingleWalletLock == true
               )
               .BatchUpdate
               (
                   new Domain.Wallets.Wallet()
                   {
                       IsSingleWalletLock = false
                   }
               );

        result = _wallet.Wallets.Any(w => w.IsSingleWalletLock == true);

        result.Should()
              .Be(false);
    }
}