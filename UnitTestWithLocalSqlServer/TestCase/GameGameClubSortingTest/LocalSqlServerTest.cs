using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using UnitTestWithSqlite.Domain.GameClubSorting;
using UnitTestWithSqlite.Domain.Model;

namespace UnitTestWithSqlite.TestCase.GameGameClubSortingTest;

public partial class SqliteTest
{
    private GameClubSortingContext _sorting;
    private SqliteConnection       _coon;

    [SetUp]
    public void Setup()
    {
        var coon  = "Server=localhost,1433;Database=PointCenter;User Id=sa;Password=1qaz@WSX;TrustServerCertificate=true;Encrypt=true";
        var billingSystemOptions = new DbContextOptionsBuilder<GameClubSortingContext>().UseSqlServer(coon);
        this._sorting = new GameClubSortingContext(billingSystemOptions.Options);
    }


    [Test]
    public async Task Test1()
    {

        {
            var param = new FrontendGameClubParam() {Id = 10, Sort = 5, ClubName = "測試J館(被修改)", ClubCode = "J(被修改)"};
            var targetIndex = param.Sort;

            _sorting.FrontendGameClubs
                    .Where(fgc => fgc.Sort >= param.Sort)
                    .BatchUpdate(fgc => new FrontendGameClub() {Sort = fgc.Sort + 1});


            var target = _sorting
                         .FrontendGameClubs
                         .SingleOrDefault(f => f.Id == param.Id);

            target.Sort     = targetIndex;
            target.ClubCode = param.ClubCode;
            target.ClubName = param.ClubName;

            await _sorting.SaveChangesAsync();


            var data = _sorting
                       .FrontendGameClubs
                       .OrderBy(fgc => fgc.Sort)
                       .ToList();

            foreach (var detail in data.Select
                     (
                         (fgc
                        , i) => new {i, fgc}
                     ))
            {
                _sorting.Entry(detail.fgc)
                        .State = EntityState.Modified;
                detail.fgc.Sort = detail.i + 1;
            }

            await _sorting.SaveChangesAsync();
        }

        Assert.Pass();
    }
}