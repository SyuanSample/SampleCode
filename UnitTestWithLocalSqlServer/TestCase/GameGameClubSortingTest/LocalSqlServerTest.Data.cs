using System.Collections.Generic;
using UnitTestWithSqlite.Domain.GameClubSorting;

namespace UnitTestWithSqlite.TestCase.GameGameClubSortingTest;

public partial class SqliteTest
{
    /// <summary>
    ///     初始化資料
    /// </summary>
    /// <returns></returns>
    private List<FrontendGameClub> DataInit()
    {
        var result = new List<FrontendGameClub>()
        {
            new FrontendGameClub {Id = 1, Sort  = 1, ClubName  = "測試A館", ClubCode = "A"},
            new FrontendGameClub {Id = 2, Sort  = 2, ClubName  = "測試B館", ClubCode = "B"},
            new FrontendGameClub {Id = 3, Sort  = 3, ClubName  = "測試C館", ClubCode = "C"},
            new FrontendGameClub {Id = 4, Sort  = 4, ClubName  = "測試D館", ClubCode = "D"},
            new FrontendGameClub {Id = 5, Sort  = 5, ClubName  = "測試E館", ClubCode = "E"},
            new FrontendGameClub {Id = 6, Sort  = 10, ClubName = "測試F館", ClubCode = "F"},
            new FrontendGameClub {Id = 7, Sort  = 9, ClubName  = "測試G館", ClubCode = "G"},
            new FrontendGameClub {Id = 8, Sort  = 8, ClubName  = "測試H館", ClubCode = "H"},
            new FrontendGameClub {Id = 9, Sort  = 7, ClubName  = "測試I館", ClubCode = "I"},
            new FrontendGameClub {Id = 10, Sort = 6, ClubName  = "測試J館", ClubCode = "J"},
        };

        return result;
    }
}