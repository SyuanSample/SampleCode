﻿namespace MessageService.Domain;

public class MsgResult
{
    /// <summary>
    ///     訊息發送的結果
    /// </summary>
    public bool Susses { get; set; } = false;

    /// <summary>
    ///     取回的內容
    /// </summary>
    public string Msg { get; set; } = string.Empty;
}