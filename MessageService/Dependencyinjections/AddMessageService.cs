﻿using MessageService.Factories;
using MessageService.Interface;
using MessageService.Unit;
using Microsoft.Extensions.DependencyInjection;

namespace MessageService.Dependencyinjections;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddMessageService(this IServiceCollection services)
    {
        services.AddHttpClient
        (
            "SlackApi",
            opt =>
            {
                opt.BaseAddress = new Uri
                    ("https://hooks.slack.com/services/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");

                opt.Timeout = TimeSpan.FromSeconds(10);
            }
        );

        services.AddScoped<MsgFactory>();
        services.AddScoped<IMessageTemplate, SlackMessage>();

        return services;
    }
}