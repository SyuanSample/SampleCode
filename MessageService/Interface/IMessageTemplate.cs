﻿using MessageService.Domain;

namespace MessageService.Interface;

public interface IMessageTemplate
{
    /// <summary>
    ///     初始化連線設定
    /// </summary>
    void InitClient();

    /// <summary>
    ///     發送訊息
    /// </summary>
    /// <param name="json"></param>
    Task<MsgResult> SendMessageAsync(string json);
}