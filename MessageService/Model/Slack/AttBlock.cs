﻿namespace MessageService.Model.Slack;

public class AttBlock
{
    
    public string type { get; set; }
    public Field[] fields { get; set; }
    public Message text { get; set; }
    public Accessory accessory { get; set; }
}