﻿namespace MessageService.Model.Slack;

public class Field
{
    
    public string type { get; set; }
    public string text { get; set; }
}