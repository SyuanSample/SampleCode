﻿namespace MessageService.Model.Slack;

public class Accessory
{
    
    public string type { get; set; }
    public Message text { get; set; }
    public string value { get; set; }
}