﻿namespace MessageService.Model.Slack;

/// <summary>
///     訊息內容
/// </summary>
public class Message
{
    /// <summary>
    ///     內容類型
    /// </summary>
    public string type { get; set; }

    /// <summary>
    ///     內容
    /// </summary>
    public string text { get; set; }
}