﻿namespace MessageService.Model.Slack;

/// <summary>
///     
/// </summary>
public class Block
{
    public string type { get; set; }
    public Message Message { get; set; }
}