﻿namespace MessageService.Model.Slack;

public class Attachment
{
    
    public string color { get; set; }
    public AttBlock[] blocks { get; set; }
}