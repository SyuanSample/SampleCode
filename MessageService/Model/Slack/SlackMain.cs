﻿namespace MessageService.Model.Slack;

public class SlackMain
{
    public string type { get; set; }

    public Block[] blocks { get; set; }

    public Attachment[] attachments { get; set; }
}