﻿using MessageService.Enums;
using MessageService.Interface;
using MessageService.Unit;

namespace MessageService.Factories;

public class MsgFactory
{
    private readonly IEnumerable<IMessageTemplate> _msg;


    public MsgFactory(IEnumerable<IMessageTemplate> msg)
    {
        _msg = msg;
        Console.WriteLine("CommonVideoFactory");
    }


    /// <summary>
    ///     設定遊戲要使用的Class
    /// </summary>
    /// <param name="msgService"></param>
    /// <returns></returns>
    private static Type Init(MsgServicesEnum msgService)
    {
        //  目前僅有Xg館有特殊規格
        //  完美目前是依照舊規格
        return msgService switch
               {
                   MsgServicesEnum.Slack => typeof(SlackMessage)
                 , _                     => throw new NullReferenceException("指定的服務類型不存在。")
               };
    }

    /// <summary>
    ///     取出對應的Service
    /// </summary>
    /// <param name="videoByGameType"></param>
    /// <returns></returns>
    public IMessageTemplate GetService(MsgServicesEnum videoByGameType)
    {
        var service = _msg.SingleOrDefault(x => x.GetType() == Init(videoByGameType));

        return service!;
    }
}