﻿using MessageService.Domain;
using MessageService.Interface;

namespace MessageService.Abstract;

public abstract class BaseMessageTemplate : IMessageTemplate
{
    /// <summary>
    ///     初始化連線設定
    /// </summary>
    public abstract void InitClient();

    /// <summary>
    ///     發送訊息
    /// </summary>
    /// <param name="json"></param>
    public abstract Task<MsgResult> SendMessageAsync(string json);
}