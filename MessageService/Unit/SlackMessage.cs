﻿using System.Net.Mime;
using System.Text;
using MessageService.Abstract;
using MessageService.Domain;
using static System.String;

namespace MessageService.Unit;

public class SlackMessage : BaseMessageTemplate
{
    private readonly IHttpClientFactory _factory;

    /// <summary>
    ///     Slack發送的Url
    /// </summary>
    private readonly Uri _slackUrl;

    public SlackMessage(IHttpClientFactory factory
                      /*, Uri                slackUrl*/)
    {
        _factory  = factory;
        //_slackUrl = slackUrl;
    }


    /// <summary>
    ///     初始化連線設定
    /// </summary>
    public override void InitClient()
    {
    }

    /// <summary>
    ///     發送訊息
    /// </summary>
    /// <param name="json"></param>
    public override async Task<MsgResult> SendMessageAsync(string json)
    {
        var content = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);
        var client  = _factory.CreateClient("SlackApi");

        var response = await client.PostAsync(Empty, content);

        var result = new MsgResult
        {
            Susses = response.IsSuccessStatusCode
          , Msg    = await response.Content.ReadAsStringAsync()
        };

        return result;
    }
}