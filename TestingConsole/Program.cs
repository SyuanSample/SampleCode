﻿// See https://aka.ms/new-console-template for more information

using System.Net.Http.Headers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var builder = new HostBuilder()
              .ConfigureServices
              (
                  (hostContext
                 , services) =>
                  {
                      services.AddHttpClient();
                  }
              )
              .UseConsoleLifetime();

var host = builder.Build();

using var serviceScope = host.Services.CreateScope();
var       services     = serviceScope.ServiceProvider;
var       client       = services.GetRequiredService<HttpClient>();
var       uri          = new Uri("https://stat.fsc.gov.tw/FSC_OAS3_RESTORE/api/CSV_EXPORT?TableID=B14&OUTPUT_FILE=Y");

using var response = await client.GetAsync(uri);
using var content  = response.Content;
using var stream   = (MemoryStream) await content.ReadAsStreamAsync();
using var sr       = new StreamReader(stream);

while (!sr.EndOfStream)
{
    var row =
        sr.ReadLine();

    if (row.IndexOf(",") == -1)
        continue;

    Console.WriteLine(row);
}

var t = 1;