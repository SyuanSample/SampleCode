﻿
namespace ObserverSample.Interface;

public interface IObservers
{
    //主題狀態 ? 
    string SubjectState { get; set; }
    //通知方法 ?
    void Notify();
}