using ObserverSample.Interface;

namespace ObserverSample.Observer;

public class NBAObserver
{
    public string     name;
    public IObservers sub;
    public NBAObserver(string name, IObservers sub)
    {
        this.name = name;
        this.sub  = sub;
    }
    public void CloseNBA()
    {
        Console.WriteLine($"通知內容：{sub.SubjectState},反應：{name}關閉NBA直播，繼續工作！");
    }
}