using ObserverSample.Interface;

namespace ObserverSample.Observer;

public class StockObserver
{
    public string     name;
    public IObservers sub;

    public StockObserver(string     name
                       , IObservers sub)
    {
        this.name = name;
        this.sub  = sub;
    }

    public void CloseStockMarket()
    {
        Console.WriteLine($"通知內容：{sub.SubjectState},反應：{name}關閉股票行情，繼續工作！");
    }
}