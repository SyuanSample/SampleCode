﻿using ObserverSample.Interface;

public abstract  class AObserver
{
    //名字
    protected string name;
    //觀察者要知道自己訂閱了那個主題
    protected IObservers sub;
    public AObserver(string name, IObservers sub)
    {
        this.name = name;
        this.sub  = sub;
    }
}