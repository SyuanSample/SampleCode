﻿// See https://aka.ms/new-console-template for more information

//  參考來源 - https://iter01.com/124530.html

using ObserverSample.Observer;
using ObserverSample.Services;

Console.WriteLine("Hello, World!");

var manager = new ObManagerServer();
var stock   = new StockObserver("巴菲特", manager);
var nba     = new NBAObserver("麥迪", manager);

manager.Update += stock.CloseStockMarket;
manager.Update += nba.CloseNBA;
//主題狀態更改，並通知
manager.SubjectState = "老闆回來了！";
manager.Notify();
Console.ReadKey();