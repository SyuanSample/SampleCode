using ObserverSample.Interface;

namespace ObserverSample.Services;

public class ObManagerServer : IObservers
{
    public delegate void EventHandler();

    public event EventHandler Update;

    public string SubjectState { get; set; }

    public void Notify()
    {
        Update();
    }
}