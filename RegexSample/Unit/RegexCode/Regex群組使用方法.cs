﻿using System.Text.RegularExpressions;
using SampleCode.Interface;

namespace SampleCode.Unit.RegexCode;

public class Regex群組使用方法 : ICommon
{

    public void Logic()
    {
        var sampleString
            = "{\"KeyWordOne\":\"abcdef12345\",\"KeyWordTwo\":\"ghijkl67890\",\"KeyWordThree\":\"lmnopq12345\"}";
        Console.WriteLine($"輸入的字串內容：{sampleString}");

        var result = Regex邏輯(sampleString);
        Console.WriteLine($"替換後的字串內容：{result}");
    }

    /// <summary>
    ///     取出Json String中特定的屬性，
    ///     並且替換掉原本的內容
    ///
    ///     可用於加密帳號密碼等機敏資料。
    /// </summary>
    /// <param name="jsonString"></param>
    /// <returns></returns>
    /// <remarks>
    ///     目前知道的問題：
    ///         如果有特殊字元會無法正常取出，因為在正則表示上特殊字元前面通常要[\^]
    ///         所以要使用的話需要確認字串的輸入格式。
    /// </remarks>>
    public string Regex邏輯(string jsonString)
    {
        var keyWords = new Regex
        (
            @"\\{0,1}""KeyWordOne\\{0,1}"":\\{0,1}""(?<KeyWordOne>[A-Za-z0-9]+)\\{0,1}""|\\{0,1}""KeyWordTwo\\{0,1}"":\\{0,1}""(?<KeyWordTwo>[A-Za-z0-9]*)\\{0,1}"""
        );

        var matches = keyWords.Matches(jsonString);

        foreach (var match in matches)
        {
            var collection = match.ToString();

            if (!string.IsNullOrWhiteSpace(collection))
            {
                var content = new Regex(@":\\{0,1}""(?<content>[A-Za-z0-9_]+)\\{0,1}\""");

                var word = content.Match(collection)
                                  .Groups[1]
                                  .Value;

                var newWord = "替換的內容";

                var regex = new Regex(word);
                jsonString = regex.Replace(jsonString, newWord);
            }
        }

        return jsonString;
    }
}